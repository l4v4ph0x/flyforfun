#include "StdAfx.h"

#include "MovPlayer.h"

MovPlayer::MovPlayer(LPDIRECT3DDEVICE9 pd3dDevice, std::string movName) {

  bool gotMov = false;

  rapidjson::Document document = JsonUtil::Open(PROJECT_STRUCTURE);
  if (document.HasMember("MOV")) {
    const rapidjson::Value& movs = document["MOV"];
    for (size_t i = 0; i < movs.Size(); i++) {

      const rapidjson::Value& name = movs[i]["Name"];
      const rapidjson::Value& path = movs[i]["Path"];
      const rapidjson::Value& frameRate = movs[i]["FrameRate"];

      if (movName == std::string(name.GetString())) {
        auto tickStart = GetTickCount();
        for (const auto & entry : fs::directory_iterator(std::string(MakePath(DIR_PROJECT, path.GetString())))) {

          if (entry.path().u8string().rfind(".jpg") == std::string::npos)
            continue;

          CTexture * tex = new CTexture();


          //tex->LoadTexture(pd3dDevice, entry.path().u8string().c_str(), 0xff0000, TRUE);

          auto fileName = entry.path().u8string();
          LPDIRECT3DTEXTURE9 _tex;
          LPDIRECT3DSURFACE9 surf;
          D3DXIMAGE_INFO imgInfo;

          //D3DXCreateTextureFromFile(pd3dDevice, fileName.c_str(), &_tex);

          pd3dDevice->CreateTexture(1280, 720, 1, 0, D3DFMT_X8R8G8B8, D3DPOOL_DEFAULT, &_tex, 0);
          //D3DXGetImageInfoFromFile(fileName.c_str(), &imgInfo);
          _tex->GetSurfaceLevel(0, &surf);
          D3DXLoadSurfaceFromFile(surf, 0, 0, fileName.c_str(), 0, D3DX_FILTER_NONE, 0, 0);
          surf->Release();

          SIZE size;
          size.cx = 1280;
          size.cy = 720;

          tex->m_pTexture = _tex;
          tex->m_size.cx = 1280;
          tex->m_size.cy = 720;

          tex->m_fuLT = 0.0f;
          tex->m_fvLT = 0.0f;

          tex->m_fuRT = (FLOAT)size.cx / tex->m_size.cx;
          tex->m_fvRT = 0.0f;

          tex->m_fuLB = 0.0f;
          tex->m_fvLB = (FLOAT)size.cy / tex->m_size.cy;

          tex->m_fuRB = (FLOAT)size.cx / tex->m_size.cx;
          tex->m_fvRB = (FLOAT)size.cy / tex->m_size.cy;

          tex->m_ptCenter.x = 0;
          tex->m_ptCenter.y = 0;

          tex->m_sizePitch = tex->m_size;
          tex->m_size = size;

          //printf("%s\n", fileName.c_str());
          frames.push_back(tex);
        }

        //frames.push_back()

        auto tickEnd = GetTickCount();
        printf("loading took time %d ms\n", tickEnd - tickStart);


        gotMov = true;
        break;
      }

    }
  }


  if (gotMov == false) {
    Error("MovPlayer failed to find %s from %s", movName.c_str(), PROJECT_STRUCTURE);
  }

}

CTexture * MovPlayer::GetNextFrame() {
  frameCounter++;

  if (frameCounter >= frames.size())
    frameCounter = 0;

  return frames[frameCounter];
}

// basically the render function
CTexture * MovPlayer::GetTickFrame() {

  if (fadeOutCallback != nullptr && GetOppacity() == 0) {
    fadeOutCallback();
    fadeOutCallback = nullptr;
  }

  auto tick = GetTickCount();
  // 24 is frame rate
  if (tick - dwLastTicksToNewFrame > 1000 / 12) {
    dwLastTicksToNewFrame = tick;
    return GetNextFrame();
  }

  // else return current frame
  return frames[frameCounter];
}

void MovPlayer::StartFadeOut(std::function<void()> callback) {
  opaccityTicks = GetTickCount();
  fadeOutCallback = callback;
}

DWORD MovPlayer::GetOppacity() {
  auto tick = GetTickCount();

  if (opaccityTicks != 0) {
    opaccity = 255 - ((tick - opaccityTicks) * opaccityFadeOutSpeed);
    if (opaccity < 0) {
      opaccity = 0;
      opaccityTicks = 0;
    }
  }

  return (DWORD)opaccity;
}


std::vector<std::string> MovPlayer::GetMovFilesFromProjectStructure() {
  std::vector<std::string> res;

  rapidjson::Document document = JsonUtil::Open(PROJECT_STRUCTURE);
  if (document.HasMember("MOV")) {
    const rapidjson::Value& movs = document["MOV"];
    for (size_t i = 0; i < movs.Size(); i++) {

      const rapidjson::Value& name = movs[i]["Name"];
      const rapidjson::Value& path = movs[i]["Path"];
      const rapidjson::Value& frameRate = movs[i]["FrameRate"];

      res.push_back(name.GetString());

    }
  }

  return res;
}