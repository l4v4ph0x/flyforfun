#pragma once

#include "StdAfx.h"
#include "2DRender.h"

class FastTextureLoader {

public:

  static CTexture * GetCTexture(const char * fileName, SIZE scaleToSize) {
    CTexture * tex = new CTexture();
    LPDIRECT3DTEXTURE9 _tex;
    LPDIRECT3DSURFACE9 surf;
    D3DXIMAGE_INFO imgInfo;

    D3DXGetImageInfoFromFile(fileName, &imgInfo);
    g_Neuz.m_pd3dDevice->CreateTexture(imgInfo.Width, imgInfo.Height, 1, 0, D3DFMT_A8R8G8B8, D3DPOOL_DEFAULT, &_tex, 0);
    _tex->GetSurfaceLevel(0, &surf);
    D3DXLoadSurfaceFromFile(surf, 0, 0, fileName, 0, D3DX_FILTER_NONE, 0, 0);
    surf->Release();

    SIZE size;
    size.cx = imgInfo.Width;
    size.cy = imgInfo.Height;

    tex->m_pTexture = _tex;
    tex->m_size = scaleToSize;

    tex->m_fuLT = 0.0f;
    tex->m_fvLT = 0.0f;

    tex->m_fuRT = (FLOAT)size.cx / tex->m_size.cx;
    tex->m_fvRT = 0.0f;

    tex->m_fuLB = 0.0f;
    tex->m_fvLB = (FLOAT)size.cy / tex->m_size.cy;

    tex->m_fuRB = (FLOAT)size.cx / tex->m_size.cx;
    tex->m_fvRB = (FLOAT)size.cy / tex->m_size.cy;

    tex->m_ptCenter.x = 0;
    tex->m_ptCenter.y = 0;

    tex->m_sizePitch = tex->m_size;
    tex->m_size = size;

    return tex;
  }

};
