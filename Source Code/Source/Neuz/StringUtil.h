#pragma once

#include <string>
#include <vector>
#include <sstream>
#include <iostream>
#include <algorithm>
#include <cctype>

class StringUtil {

public:

  static std::string RemoveCharsFromEndTil(std::string str, std::string needle) {
    std::string res = str;

    for (auto i = res.size() - 1; i > needle.size(); i--) {
      if (res.substr(i - needle.size()).rfind(needle) != std::string::npos) {
        return res.substr(0, i);
      }
    }

    return "";
  }

  static std::string RemoveCharsFromEndTilSlash(std::string str) {
    std::string res = str;

    for (auto i = res.size() - 1; i > 1; i--) {
      if (res.substr(i - 1).rfind("/") != std::string::npos || res.substr(i - 1).rfind("\\") != std::string::npos) {
        return res.substr(0, i);
      }
    }

    return "";
  }

  static std::string CopyToString(const char *chrArr) {
    std::string res = "";

    for (auto i = 0; i < strlen(chrArr); i++) {
      res += chrArr[i];
    }

    return res;
  }

  static std::vector<std::string> Split(std::string str, const char * delim) {
    std::stringstream ss(str);
    std::istream_iterator<std::string> begin(ss);
    std::istream_iterator<std::string> end;
    std::vector<std::string> vstrings(begin, end);
    std::copy(vstrings.begin(), vstrings.end(), std::ostream_iterator<std::string>(std::cout, delim));

    return vstrings;
  }

  static int HexToInt(std::string str) {
    unsigned int x;
    std::stringstream ss;
    ss << std::hex << ((str.rfind("0x") == 0) ? str.substr(2) : str);
    ss >> x;

    return x;
  }

  static std::string ToLowerCase(std::string str) {
    std::string data = str;
    std::transform(data.begin(), data.end(), data.begin(), [](unsigned char c) { return std::tolower(c); });

    return data;
  }

};