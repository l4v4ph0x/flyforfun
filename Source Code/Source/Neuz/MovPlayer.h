#pragma once

#include <vector>
#include <string>
#include <filesystem>
#include <functional>

#include "JsonUtil.h"

namespace fs = std::experimental::filesystem;

class MovPlayer {

  size_t frameCounter = 0;
  std::vector<CTexture *> frames;
  DWORD dwLastTicksToNewFrame = 0;


  DWORD opaccityTicks = 0;
  float opaccityFadeOutSpeed = 0.5;
  int opaccity = 255;
  std::function<void()> fadeOutCallback;

public:


  MovPlayer(LPDIRECT3DDEVICE9 pd3dDevice, std::string movName);


  CTexture * GetNextFrame();

  // basically the render function
  CTexture * GetTickFrame();

  void StartFadeOut(std::function<void()> callback = nullptr);

  DWORD GetOppacity();


  static std::vector<std::string> GetMovFilesFromProjectStructure();

};

