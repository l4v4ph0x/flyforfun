#pragma once

#include "stdafx.h"

#include <rapidjson\document.h>

class JsonUtil {

public:
  static rapidjson::Document Open(const char * location) {
    std::string jsonString = CResFile::ReadFileToString(location);

    rapidjson::Document document;
    document.Parse(jsonString.c_str());

    return document;
  }

};