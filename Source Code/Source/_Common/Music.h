#ifndef __MUSIC_H__
#define __MUSIC_H__

#include <string>
#include <map>

class CMusicMng
{

public:

#ifdef __LOAD_FROM_PROJECT_FOLDER_V1
  std::map<std::string, std::string> nameToFileMusicMap;
#endif

	struct MUSIC
	{
		TCHAR szMusicFileName[ 128 ];
	};
	CFixedArray< MUSIC > m_aMusic;

	BOOL LoadScript( LPCTSTR lpszFileName );
	LPTSTR GetFileName( DWORD dwId );

#ifdef __LOAD_FROM_PROJECT_FOLDER_V1
  void LoadFromCustomConfig();
#endif

};
void ProcessFadeMusic();
BOOL PlayMusic( LPCTSTR lpszFileName, int nLoopCount = 1 );

#ifdef __LOAD_FROM_PROJECT_FOLDER_V1
BOOL PlayMusicFromProject(std::string name, int nLoopCount = 1);
#endif

BOOL PlayMusic( DWORD dwIdMusic, int nLoopCount = 1 );
BOOL PlayBGM( DWORD dwIdMusic );
void LockMusic();
void StopMusic();
void SetVolume( FLOAT fVolume );
FLOAT GetVolume();
BOOL IsPlayingMusic();
BOOL IsStopMusic();
void InitCustomSound( BOOL bEnable = TRUE );
void UnInitCustomSound();

extern CMusicMng g_MusicMng;



#endif
